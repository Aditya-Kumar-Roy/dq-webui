<!DOCTYPE html>
<html>
<head>
    <title>Exadatum's Data Quality Tool</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <link rel="stylesheet" type="text/css" href="../commons/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../commons/css/keen-dashboards.css" />
    <link rel="stylesheet" type="text/css" href="schedule-dq-models.css" />
    <link rel="stylesheet" type="text/css" href="add-columns.js" />
</head>
<body class="application">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="">
                <img src="../../img/exa-logo.png" width="20">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="navbar-brand" href="./">Exadatum's Data Quality Tool</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="../index-files/index.html">My DashBoard</a></li>
                <li><a href="../register-dq-model/register-dq-model.html">Register DQ Model</a></li>
                <li><a href="../past-dq-models/past-dq-models.php">Past DQ Model</a></li>
                <li><a href="../schedule-dq-models/scheduled-models.html">Scheduled DQ Model</a></li>
                <li><a href="../undercons.html">History Logs</a></li>
                <li><a href="http://35.154.225.107:5601/app/kibana#/management/kibana/index/?_g=()"> Kibana Dashboard</a> </li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <br><br> <br> <br> <br>
    <fieldset >
        <legend> Scheduled DQ Models </legend>

        <hr>
        <?php
            $constraints_dictionary["1"] = "NULL";
            $constraints_dictionary["2"] = "NOT_NULL";
            $constraints_dictionary["3"] = "UNIQUE_KEY";
            $constraints_dictionary["4"] = "MATCHING_REGEX";
            $constraints_dictionary["5"] = "SATISFIES_STRING";
            $constraints_dictionary["7"] = "NUM_ROWS";
            $constraints_dictionary["8"] = "EQUALLABLE_WITH";
            $constraints_dictionary["9"] = "ANY_OF";
            $constraints_dictionary["10"] = "FUNCTIONAL_DEPENDENCY";
            $constraints_dictionary["11"] = "FORMATTED_DATE";
            $constraints_dictionary["12"] = "CONVERTABLE_DATATYPE";
            $constraints_dictionary["13"] = "FOREIGN_KEY";
            $constraints_dictionary["14"] = "JOINABLE_WITH";

            if($_SERVER["REQUEST_METHOD"] == "POST") {

            $con = mysqli_connect('127.0.0.1','root','','dq_webui');
            if (!$con) {
                die('Could not connect to the mysql Instance' . mysqli_error($con));
            }

            mysqli_select_db ($con,"dq_webui");
            $execution_id = $_POST['execution_id'];
            $sql="SELECT * FROM historical_extracts where executionId=".$execution_id;
            $result = mysqli_query($con,$sql);
            while($row = mysqli_fetch_array($result)) {
                $schema_detailed = $row['modelSchema'];
            }

        ?>
            <form name="schedule_dq_model" action="schedule-dq-model-intermediate.php" method="post">
            <table id="table2" border="0" height="100%" width="100%" align="center" class="demo-table">
                <tr>
                    <td align="center"> <strong> Chose a Name for the DQ Model </strong> </td>
                    <td align="center">
                        <input type="hidden" name="execution_Id" value="<?php echo $execution_id ?>" />
                        <input type="text" size="50px" class="demoInputBox" name="model_name" placeholder="Chose a Name for the Model">
                    </td>
                </tr>
                <tr>
                    <td align="center"> <strong> Schema Definition </strong> </td>
                    <td>
                        <input type="hidden" name="schema_json" value='<?php echo $schema_detailed ?>' />
                    <table id="table3" border="1" width="400" align="center" class="demo-table-small">
                        <tr>
                        <th> <center> <strong> Table Name </strong> </center> </th>
                        <th> <center> <strong> Column Name </strong> </center> </th>
                        <th> <center> <strong>  Constraint Applied </strong> </center> </th>
                        <th> <center> <strong> Parameters passed </strong> </center> </th>
                        </tr>
                        <?php

                        $json_array = json_decode($schema_detailed, true);

                        for ($i = 0; $i < sizeof($json_array); $i++) {
                          $tableName = $json_array[$i]['dataSet'];

                          $validityReq = $json_array[$i]['validityReq'];

                          for ($j = 0; $j < sizeof($validityReq); $j++) {
                            $columnName = $validityReq[$j]['colName'];
                            $metrics = $validityReq[$j]['metrics'];

                            for ($k = 0; $k < sizeof($metrics); $k++) {
                              $constraintName = $metrics[$k]['name'];
                              $parameters = $metrics[$k]['value'];
                        ?>
                        <tr>
                            <td align="center"> <?php echo $tableName; ?> </td>
                            <td align="center"> <?php echo $columnName; ?> </td>
                            <td align="center"> <?php echo $constraints_dictionary[$constraintName]; ?> </td>
                            <td align="center"> <?php echo $parameters; ?> </td>
                        </tr>
                        <?php } } } }?>
                    </table>
                    </td>

                    <!--<td align="center">  <input type="button" size="50px" value="Chose Schema" name="addColumn" class="btnAddColumn"> </td>-->
                </tr>
                <tr>
                    <td align="center"> <strong> Scheduling Model </strong> </td>
                    <td align="center">
                        <select name="scheduling_type" style="size: 50px">
                            <option value="HOURLY"> HOURLY </option>
                            <option value="DAILY"> DAILY </option>
                            <option value="WEEKLY"> WEEKLY </option>
                            <option value="MONTHLY"> MONTHLY </option>
                            <option value="QUARTERLY"> QUARTERLY </option>
                            <option value="SEASONAL"> SEASONAL </option>
                            <option value="YEARLY"> YEARLY </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="center"> <strong> Scheduled period  </strong> </td>
                    <td align="center"> <input type="text" size="50px" class="demoInputBox" name="schedule_period" placeholder="Scheduling time frame - w for weeks, d for days"> </td>
                </tr>
                <tr>
                    <td align="center"> <strong> Scheduled Time (HH,mm,am/pm,timezone)  </strong> </td>
                    <td align="center"> <input type="text" size="50px" class="demoInputBox" name="schedule_time" placeholder="Time of Scheduling"> </td>
                </tr>
                <tr>
                    <td align="center"> <strong> Scheduled Start Date (mm/dd/yyyy)  </strong> </td>
                    <td align="center"> <input type="text" size="50px" class="demoInputBox" name="schedule_date" placeholder="Start Date of Scheduling"> </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Schedule for DQ Checks" class="button" style="width:300px; margin: 0 auto; text-align:center" />
                    </td>
                </tr>
            </table>
            </form>
    </fieldset>

    <hr>
    <p class="small text-muted">Built with &#9829; by <a href="https://www.exadatum.com">Exadatum Software Services Pvt. Ltd</a></p>

</div>

<script type="text/javascript" src="../commons/jquery.min.js"></script>
<script type="text/javascript" src="../commons/bootstrap.min.js"></script>

<script type="text/javascript" src="../commons/holder.js"></script>
<script>
    Holder.add_theme("white", { background:"#fff", foreground:"#a7a7a7", size:10 });
</script>

<script type="text/javascript" src="../commons/keen.min.js"></script>
<script type="text/javascript" src="../commons/meta.js"></script>
<script type="text/javascript" src="../commons/keen.dashboard.js"></script>
</body>
</html>
