<?php
    if($_SERVER["REQUEST_METHOD"] == "POST") {

        $DQ_MODEL_NAME = $_POST['model_name'];
        $DQ_SCHEMA_JSON = $_POST['schema_json'];
        $DQ_SCHEDULING_TYPE = $_POST['scheduling_type'];
        $DQ_PERIOD = $_POST['schedule_period'];
        $DQ_TIME = $_POST['schedule_time'];
        $DQ_DATE = $_POST['schedule_date'];
        $EXEC_ID = $_POST['execution_Id'];

        //Default Setup
        echo $DQ_MODEL_NAME."<br/>";
        echo $DQ_SCHEMA_JSON."<br/>";
        echo $DQ_SCHEDULING_TYPE."<br/>";
        echo $DQ_PERIOD."<br/>";
        echo "$DQ_TIME"."<br/>";
        echo "$DQ_DATE"."<br/>";
        echo "Execution ID = ".$EXEC_ID."<br/>";

        // Rest Call to the Specified Azkaban URL :
        $Login_URL = 'http://35.154.225.107:8081';
        $Login_Data = array('action' => 'login', 'username' => 'azkaban', 'password' => 'azkaban');

        // use key 'http' even if you send the request to https://...
        $options = array( 'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST','content' => http_build_query($Login_Data)
                        )   );
        $result = file_get_contents($Login_URL, false, stream_context_create($options));

        $session_details = json_decode($result, true);

        echo "SESSSION_DETAILS = ".$session_details;

         if ($session_details["status"] == 'success') {
            $session_id = $session_details["session.id"];

            echo "SESSION_ID = ".$session_id."<br/>";

            //Fetch Project id ...
            //35.154.225.107:8081/manager?ajax=fetchprojectflows&project=DQ_Web_UI&session.id=6ec739aa-06b3-4add-bfe0-ff343a97994c
            /*
            $FETCH_PROJ_ID_URL = $Login_URL."/manager";
            echo "Project URL = ".$FETCH_PROJ_ID_URL."<br/>";
            $PROJ_ID_DATA = array('ajax' => 'fetchprojectflows', 'session.id' => $session_id, 'project' => 'DQ_Web_UI');

            $PROJ_ID_OPTIONS = array( 'http' => array(
                            'header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST',
                            'content' => http_build_query($PROJ_ID_DATA)    )   );

            $PROJ_ID_RESULT = file_get_contents($FETCH_PROJ_ID_URL, false, stream_context_create($PROJ_ID_OPTIONS));
            $PROJ_ID_JSON = json_decode($PROJ_ID_RESULT, true);
            echo "<br/>".$PROJ_ID_JSON;
            $PROJ_ID = $PROJ_ID_JSON['projectId'];
            echo "<br/> PROJECT_ID = ".$PROJ_ID."<br/>";
            */

            $FLOW_SCHEDULE_URL = "http://35.154.225.107:8081/schedule";
            $FLOW_SCHEDULE_DATA = array('ajax' => 'scheduleFlow', 'is_recurring' => 'on', 'period' => $DQ_PERIOD, 'flow' => 'of-dq-runner', 'projectId' => '33' , 'projectName' => 'DQ_Web_UI', 'scheduleTime' => $DQ_TIME, 'scheduleDate' => $DQ_DATE, 'session.id' => $session_id  );
            $FLOW_SCHEDULE_OPTIONS = array('http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST', 'content' => http_build_query($FLOW_SCHEDULE_DATA)
                )   );

            $FLOW_TRIGGER_RESULT = file_get_contents($FLOW_SCHEDULE_URL, false, stream_context_create($FLOW_SCHEDULE_OPTIONS));
            $FLOW_TRIGGER_JSON = json_decode($FLOW_TRIGGER_RESULT, true);
            echo "<br/>"."JSON RESPONSE = ".$FLOW_TRIGGER_JSON."<br/>";
            echo "RESULT = ".$FLOW_TRIGGER_JSON['status'];

            $con = mysqli_connect('127.0.0.1','root','','dq_webui');
            if (!$con) {
                die('Could not connect to the mysql Instance' . mysqli_error($con));
            }

            $sql = "INSERT INTO dq_webui.scheduled_flows (executionId, flow_name, project_name, model_name,  submitted_by, first_scheduled_run,
            repeats_every, has_sla) VALUES ($EXEC_ID, 'of-dq-runner', 'DQ_Web_UI', '$DQ_MODEL_NAME', 'admin', '$DQ_DATE', '$DQ_PERIOD', 'FALSE') ";

            if ($con->query($sql) === TRUE) {
                //Take it to the Scheduled Models Page
                header("location:scheduled-models.php");
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

    }
    }
?>