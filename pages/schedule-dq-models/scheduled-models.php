<!DOCTYPE html>
<html>
<head>
    <title>Exadatum's Data Quality Tool</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <link rel="stylesheet" type="text/css" href="../commons/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../commons/css/keen-dashboards.css" />
    <link rel="stylesheet" type="text/css" href="scheduled-models.css" />
    <link rel="stylesheet" type="text/css" href="add-columns.js" />
</head>
<body class="application">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="">
                <img src="../../img/exa-logo.png" width="20">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="navbar-brand" href="./">Exadatum's Data Quality Tool</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="../index-files/index.html">My DashBoard</a></li>
                <li><a href="../register-dq-model/register-dq-model.html">Register DQ Model</a></li>
                <li><a href="../past-dq-models/past-dq-models.php">Past DQ Model</a></li>
                <li><a href="scheduled-models.php">Scheduled DQ Model</a></li>
                <li><a href="../undercons.html">History Logs</a></li>
                <li><a href="http://35.154.225.107:5601/app/kibana#/management/kibana/index/?_g=()"> Kibana Dashboard</a> </li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <br><br> <br> <br> <br>
    <fieldset >
        <legend> Scheduled Data Quality Models </legend>

        <hr>

        <table id="table1" border="0" height="100%" width="100%" align="center" class="demo-table">
            <tr>
                <th><center><strong> Execution Id </strong></center> </th>
                <th><center><strong> Flow Name </strong></center></th>
                <th><center><strong> Project Name </strong></center></th>
                <th><center><strong> Model Name </strong></center></th>
                <th><center><strong> Submitted By </strong></center></th>
                <th><center><strong> First Scheduled Run </strong></center></th>
                <th><center><strong> Repeats Every </strong></center></th>
                <th><center><strong> HAS SLA </strong></center></th>
                <th><center><strong> Actions </strong></center></th>
                <th></th>
            </tr>

        <?php
            $con = mysqli_connect('127.0.0.1','root','','dq_webui');
            if (!$con) {
                die('Could not connect to the mysql Instance' . mysqli_error($con));
            }

            mysqli_select_db ($con,"dq_webui");
            $execution_id = $_POST['execution_id'];
            $sql="SELECT * FROM scheduled_flows";
            $result = mysqli_query($con,$sql);
            while($row = mysqli_fetch_array($result)) {
        ?>
            <tr>
                <td align="center"> <?php echo $row['executionId']; ?> </td>
                <td align="center"> <?php echo $row['flow_name']; ?> </td>
                <td align="center"> <?php echo $row['project_name']; ?> </td>
                <td align="center"> <?php echo $row['model_name']; ?> </td>
                <td align="center"> <?php echo $row['submitted_by']; ?> </td>
                <td align="center"> <?php echo $row['first_scheduled_run']; ?> </td>
                <td align="center"> <?php echo $row['repeats_every']; ?> </td>
                <td align="center"> <?php echo $row['has_sla']; ?> </td>
                <td align="center">
                    <button class="button" style="width: 160px; margin: 0 auto; text-align:center" value="REMOVE_SCHED"> Remove Schedule </button>
                    <button class="button" style="width:160px; margin: 0 auto; text-align:center" value="SET_SLQ"> Set SLA Period </button>
                </td>
            </tr>
            <?php } ?>

        </table>

    </fieldset>

</div>

    <hr>
    <p class="small text-muted">Built with &#9829; by <a href="https://www.exadatum.com">Exadatum Software Services Pvt. Ltd</a></p>

</div>

<script type="text/javascript" src="../commons/jquery.min.js"></script>
<script type="text/javascript" src="../commons/bootstrap.min.js"></script>

<script type="text/javascript" src="../commons/holder.js"></script>
<script>
    Holder.add_theme("white", { background:"#fff", foreground:"#a7a7a7", size:10 });
</script>

<script type="text/javascript" src="../commons/keen.min.js"></script>
<script type="text/javascript" src="../commons/meta.js"></script>
<script type="text/javascript" src="../commons/keen.dashboard.js"></script>
</body>
</html>
