<!DOCTYPE html>
<html>
<head>
    <title>Exadatum's Data Quality Tool</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <link rel="stylesheet" type="text/css" href="../commons/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../commons/css/keen-dashboards.css" />
    <link rel="stylesheet" type="text/css" href="past-dq-models.css" />
    <link rel="stylesheet" type="text/css" href="add-columns.js" />
</head>
<body class="application">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="">
                <img src="../../img/exa-logo.png" width="20">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="navbar-brand" href="./">Exadatum's Data Quality Tool</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="../index-files/index.html">My DashBoard</a></li>
                    <li><a href="../register-dq-model/register-dq-model.html">Register DQ Model</a></li>
                    <li><a href="past-dq-models.php">Past DQ Model</a></li>
                    <li><a href="../schedule-dq-models/scheduled-models.php">Scheduled DQ Model</a></li>
                    <li><a href="../undercons.html">History Logs</a></li>
                    <li><a href="http://35.154.225.107:5601/app/kibana#/management/kibana/index/?_g=()"> Kibana Dashboard</a> </li>
                    <li><a href="">Logout</a></li>
                </ul>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <br><br> <br> <br> <br>
    <?php
        $constraints_dictionary["1"] = "NULL";
        $constraints_dictionary["2"] = "NOT_NULL";
        $constraints_dictionary["3"] = "UNIQUE_KEY";
        $constraints_dictionary["4"] = "MATCHING_REGEX";
        $constraints_dictionary["5"] = "SATISFIES_STRING";
        $constraints_dictionary["7"] = "NUM_ROWS";
        $constraints_dictionary["8"] = "EQUALLABLE_WITH";
        $constraints_dictionary["9"] = "ANY_OF";
        $constraints_dictionary["10"] = "FUNCTIONAL_DEPENDENCY";
        $constraints_dictionary["11"] = "FORMATTED_DATE";
        $constraints_dictionary["12"] = "CONVERTABLE_DATATYPE";
        $constraints_dictionary["13"] = "FOREIGN_KEY";
        $constraints_dictionary["14"] = "JOINABLE_WITH";

        if($_SERVER["REQUEST_METHOD"] == "POST") {
         $execution_id = $_POST['schema_history_execId'];
        }
        else {
         $execution_id = '';
        }

        $con = mysqli_connect('127.0.0.1','root','','dq_webui');
        if (!$con) {
            die('Could not connect to the mysql Instance' . mysqli_error($con));
        }

        mysqli_select_db ($con,"dq_webui");
        $sql="SELECT * FROM historical_extracts where executionId=$execution_id";
        $result = mysqli_query($con,$sql);

    ?>
    <fieldset >
        <legend> Historical DQ Models - Schema Information </legend>
        <hr>
        <table id="table1" border="1" width="500" align="center" class="demo-table">



            <tr>
                <th> <center> <strong> Execution Id </strong> </center> </th>
                <th> <center> <strong> Model Name </strong> </center> </th>
                <!--<th> <center> <strong> Model Schema </strong> </center> </th>-->
                <th> <center> <strong> Scheduling Type </strong> </center> </th>
                <th> <center> <strong> Scheduled On </strong> </center> </th>
            </tr>
            <?php while($row = mysqli_fetch_array($result)) { $schema_detailed = $row['modelSchema'];

            ?>
            <tr>
                <td align="center"> <?php echo $row['executionId'] ?>  </td>
                <td align="center"> <strong> <?php echo $row['modelName'] ?> </strong></td>
                <td align="center"> <?php echo $row['schedulingType'] ?> </td>
                <td align="center"> <?php echo $row['lastRunOn'] ?> </td>
            </tr>
            <?php  } ?>
        </table>
        <hr/>
        <table id="table2" border="1" width="500" align="center" class="demo-table">
            <tr>
                <th> <center> <strong> Table Name </strong> </center> </th>
                <th> <center> <strong> Column Name </strong> </center> </th>
                <th> <center> <strong>  Constraint Applied </strong> </center> </th>
                <th> <center> <strong> Parameters passed </strong> </center> </th>
            </tr>
            <?php

            $json_array = json_decode($schema_detailed, true);

            for ($i = 0; $i < sizeof($json_array); $i++) {
              $tableName = $json_array[$i]['dataSet'];

              $validityReq = $json_array[$i]['validityReq'];

              for ($j = 0; $j < sizeof($validityReq); $j++) {
                $columnName = $validityReq[$j]['colName'];
                $metrics = $validityReq[$j]['metrics'];

                for ($k = 0; $k < sizeof($metrics); $k++) {
                  $constraintName = $metrics[$k]['name'];
                  $parameters = $metrics[$k]['value'];
                ?>

            <tr>
                <td align="center"> <?php echo $tableName; ?> </td>
                <td align="center"> <?php echo $columnName; ?> </td>
                <td align="center"> <?php echo $constraints_dictionary[$constraintName]; ?> </td>
                <td align="center"> <?php echo $parameters; ?> </td>
            </tr>

        <?php     } } }      ?>

        </table>
    </fieldset>

    <hr>
    <p class="small text-muted">Built with &#9829; by <a href="https://www.exadatum.com">Exadatum Software Services Pvt. Ltd</a></p>

</div>

<script type="text/javascript" src="../commons/jquery.min.js"></script>
<script type="text/javascript" src="../commons/bootstrap.min.js"></script>

<script type="text/javascript" src="../commons/holder.js"></script>
<script>
    Holder.add_theme("white", { background:"#fff", foreground:"#a7a7a7", size:10 });
</script>

<script type="text/javascript" src="../commons/keen.min.js"></script>
<script type="text/javascript" src="../commons/meta.js"></script>
<script type="text/javascript" src="../commons/keen.dashboard.js"></script>

</body>
</html>
