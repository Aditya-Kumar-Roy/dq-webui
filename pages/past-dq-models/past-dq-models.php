<!DOCTYPE html>
<html>
<head>
    <title>Exadatum's Data Quality Tool</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <link rel="stylesheet" type="text/css" href="../commons/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../commons/css/keen-dashboards.css" />
    <link rel="stylesheet" type="text/css" href="past-dq-models.css" />
    <link rel="stylesheet" type="text/css" href="add-columns.js" />
</head>
<body class="application">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="">
                <img src="../../img/exa-logo.png" width="20">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="navbar-brand" href="./">Exadatum's Data Quality Tool</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="../index-files/index.html">My DashBoard</a></li>
                    <li><a href="../register-dq-model/register-dq-model.html">Register DQ Model</a></li>
                    <li><a href="past-dq-models.html">Past DQ Model</a></li>
                    <li><a href="../schedule-dq-models/scheduled-models.php">Scheduled DQ Model</a></li>
                    <li><a href="../undercons.html">History Logs</a></li>
                    <li><a href="http://35.154.225.107:5601/app/kibana#/management/kibana/index/?_g=()"> Kibana Dashboard</a> </li>
                    <li><a href="">Logout</a></li>
                </ul>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <br><br> <br> <br> <br>
    <?php
        $con = mysqli_connect('127.0.0.1','root','','dq_webui');
        if (!$con) {
            die('Could not connect to the mysql Instance' . mysqli_error($con));
        }

        mysqli_select_db ($con,"dq_webui");
        $sql="SELECT * FROM historical_extracts";
        $result = mysqli_query($con,$sql);
    ?>
    <fieldset >
        <legend> Historical DQ Model's Schema </legend>
        <hr>
            <table id="table1" border="1" width="500" align="center" class="demo-table">

                <tr>
                <th> <center> <strong> Execution Id </strong> </center> </th>
                <th> <center> <strong> Model Name </strong> </center> </th>
                <th> <center> <strong> Model Schema </strong> </center> </th>
                    <th> <center> <strong> Scheduling Type </strong> </center> </th>
                    <th> <center> <strong> Scheduled On </strong> </center> </th>
                    <th> <center> <strong> Actions </strong> </center> </th>
                </tr>
                <?php while($row = mysqli_fetch_array($result)) { ?>
                <tr>
                    <td align="center"> <?php echo $row['executionId'] ?>  </td>
                    <td align="center"> <strong> <?php echo $row['modelName'] ?> </strong></td>
                    <td align="center">
                        <form name="detailed_schema_view" action="past-dq-models-schema.php" method="post">
                            <input type="hidden" name="schema_history_execId" value="<?php echo $row['executionId'] ?>">
                            <input type="submit" value="Detailed Schema" class="button" style="width:300px; margin: 0 auto; text-align:center" />
                        </form>
                        </form>
                    </td>
                    <td align="center"> <?php echo $row['schedulingType'] ?> </td>
                    <td align="center"> <?php echo $row['lastRunOn'] ?> </td>
                    <td align="center" style="width: 320px;">
                    <table border="0" width="320px" align="center" class="demo-table">
                       <tr>
                        <td> <form name="trigger_dq_flow" action="trigger-dq-model.php" method="post">
                            <input type="hidden" name="execution_id" value="<?php echo $row['executionId'] ?>" />
                        <button class="button" style="width: 80px; margin: 0 auto; text-align:center" value="Trigger"> Trigger </button>
                        </form> </td>
                        <td> <form name="schedule_dq_flow" action="../schedule-dq-models/schedule-dq-models.php" method="post">
                            <input type="hidden" name="execution_id" value="<?php echo $row['executionId'] ?>" />
                            <button type="submit" class="button" style="width: 80px; margin: 0 auto; text-align:center" value="Schedule" > Schedule </button>
                        </form> </td>
                        <td> <form name="dq_flow_logs" action="../view-dq-logs/view-dq-logs.php" method="post">
                        <input type="hidden" name="execution_id" value="<?php echo $row['executionId'] ?>" />
                        <button type="submit" class="button" style="width:80px; margin: 0 auto; text-align:center" value="Logs"> Logs</button>
                        </form> </td>
                        </tr>
                        </table>
                    </td>
                </tr>
                <?php  } ?>
            </table>
    </fieldset>

        <hr>
    <p class="small text-muted">Built with &#9829; by <a href="https://www.exadatum.com">Exadatum Software Services Pvt. Ltd</a></p>

</div>

<script type="text/javascript" src="../commons/jquery.min.js"></script>
<script type="text/javascript" src="../commons/bootstrap.min.js"></script>

<script type="text/javascript" src="../commons/holder.js"></script>
<script>
    Holder.add_theme("white", { background:"#fff", foreground:"#a7a7a7", size:10 });
</script>

<script type="text/javascript" src="../commons/keen.min.js"></script>
<script type="text/javascript" src="../commons/meta.js"></script>
<script type="text/javascript" src="../commons/keen.dashboard.js"></script>
</body>
</html>
