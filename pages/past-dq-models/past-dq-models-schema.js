var schema_string = '[{"validityReq":[{"colName":"ingestion_time","metrics":[{"name":1,"value":0},{"value":0},{"name":4,"value":"d{3]-d{3}-d{2}"},{"name":5,"value":"Allen"},{"name":7,"value":"+@4000"}],"colId":0}],"dataSet":"db_raw.of_shipments"},{"validityReq":[{"colName":"ship_node","metrics":[{"name":1,"value":0},{"name":3,"value":0},{"name":4,"value":"\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b"},{"name":5,"value":"Allen"},{"name":7,"value":"+@4000"}],"colId":1}],"dataSet":"db_insight.of_kpi_final"}]';
document.write("Schema String = " + schema_string);
var json_schema = JSON.parse(schema_string);

for (var i = 0; i < json_schema.length; ++i){
    document.write("Iteration No " + (i+1));
    var tableName = json_schema[i]["dataSet"];
    var validity_req = JSON.parse(json_schema[i]["validityReq"]);
    for (var j = 0; j < validity_req.length; ++j) {
        var columnName = validity_req[j]["colName"];
        var metrics = JSON.parse(validity_req[j]["metrics"]);
        for (var k = 0; k < metrics.length; ++k ) {
            var constraintId = metrics[k]["name"];
            var constraintExtraParameter = metrics[k]["value"];
            document.write("<tr>");
            document.write("<td>" + tableName + "</td>");
            document.write("<td>" + columnName + "</td>");
            document.write("<td>" + constraintId + "</td>");
            document.write("<td>" + constraintExtraParameter + "</td>");
            document.write("</tr>");
        }
    }
}