<!DOCTYPE html>
<html>
<head>
    <title>Exadatum's Data Quality Tool</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <link rel="stylesheet" type="text/css" href="../commons/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../commons/css/keen-dashboards.css" />
    <link rel="stylesheet" type="text/css" href="view-dq-logs.css" />
    <link rel="stylesheet" type="text/css" href="add-columns.js" />
</head>
<body class="application">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="">
                <img src="../../img/exa-logo.png" width="20">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="navbar-brand" href="./">Exadatum's Data Quality Tool</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="../index-files/index.html">My DashBoard</a></li>
                <li><a href="../register-dq-model/register-dq-model.html">Register DQ Model</a></li>
                <li><a href="../past-dq-models/past-dq-models.php">Past DQ Model</a></li>
                <li><a href="../schedule-dq-models/scheduled-models.php">Schedule DQ Model</a></li>
                <li><a href="../undercons.html">History Logs</a></li>
                <li><a href="http://35.154.225.107:5601/app/kibana#/management/kibana/index/?_g=()"> Kibana Dashboard</a> </li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <br><br> <br> <br> <br>
        <legend> Viewing DQ LOGS for Execution ID =  <?php echo $_POST['execution_id'] ?> </legend>

        <?php

            if($_SERVER["REQUEST_METHOD"] == "POST") {
             $DQ_LOGS_EXEC_ID = $_POST['execution_id'];
             $Login_URL = 'http://35.154.225.107:8081';
             $Login_Data = array('action' => 'login', 'username' => 'azkaban', 'password' => 'azkaban');

             $options = array( 'http' => array(
             'header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST','content' => http_build_query($Login_Data)
             )   );
             $result = file_get_contents($Login_URL, false, stream_context_create($options));
             $session_details = json_decode($result, true);

             if ($session_details["status"] == 'success') {
                 $session_id = $session_details["session.id"];

                 $FETCH_DETAILS_URL = 'http://35.154.225.107:8081/executor';
                 $FETCH_DETAILS_DATA = array('ajax' => 'fetchExecJobLogs', 'session.id' => $session_id, 'execid' => $DQ_LOGS_EXEC_ID , 'jobId' => 'of-dq-runner', 'offset' => '0', 'length' => '1000000000');
                 $FETCH_DETAILS_OPTIONS = array('http' => array(
                 'header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST', 'content' => http_build_query($FETCH_DETAILS_DATA)
                 )   );

                 $FETCH_DETAILS_RESULT = file_get_contents($FETCH_DETAILS_URL, false, stream_context_create($FETCH_DETAILS_OPTIONS));
                 $FETCH_DETAILS_JSON = json_decode($FETCH_DETAILS_RESULT, true);

                 $JOB_LOGS = nl2br($FETCH_DETAILS_JSON['data']);
             }
        ?>

        <div id="" style="overflow:scroll; height:700px; border: #ff6857; border: 2px;">
            <?php echo $JOB_LOGS; ?>
        </div>

        <?php } else { ?>
            <div id="logs-scroller" style="overflow:scroll; height:700px;">
                <p> Nothing to Display ! </p>
            </div>
        <?php } ?>
    </fieldset>

    <hr>
    <p class="small text-muted">Built with &#9829; by <a href="https://www.exadatum.com">Exadatum Software Services Pvt. Ltd</a></p>

</div>

<script type="text/javascript" src="../commons/jquery.min.js"></script>
<script type="text/javascript" src="../commons/bootstrap.min.js"></script>

<script type="text/javascript" src="../commons/holder.js"></script>
<script>
    Holder.add_theme("white", { background:"#fff", foreground:"#a7a7a7", size:10 });
</script>

<script type="text/javascript" src="../commons/keen.min.js"></script>
<script type="text/javascript" src="../commons/meta.js"></script>
<script type="text/javascript" src="../commons/keen.dashboard.js"></script>
</body>
</html>
