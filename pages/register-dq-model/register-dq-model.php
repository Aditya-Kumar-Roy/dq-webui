<?php

    if($_SERVER["REQUEST_METHOD"] == "POST") {

        $tableName = $_POST['tableName'];
        //echo implode(", ", $tableName[0]);
        //echo $tableName[0];

        $tabular_data= json_encode(array_values($tableName));
        echo $tabular_data;
        echo "<br/>";
        $i=0;

        //Writing to File in PHP
        //TODO : No more writing to files ....
        //$file_to_write = "dq_files/raw_csv.csv";
        //Delete the file if exists
        //$handle = fopen($file_to_write, 'w') or die("Cannot open File : ".$file_to_write);

        foreach ($tableName as $item) {
            $j=0;
            $column_names = $_POST['column_names'];
            echo $item;

            $itr=0;
            foreach ($column_names as $column_name) {
                $str="";
                $str = $item."-@-";
                $j++;
                echo $column_name;
                $str=$str.$column_name;

                if ( !empty($_POST['null']) && isset($_POST['null'][$itr]) && $_POST['null'][$itr] != 'DEFAULT')
                        $str = $str."-@-".json_decode(json_encode($_POST['null'][$itr]));

                if ( !empty($_POST['not_null']) && isset($_POST['not_null'][$itr]) && $_POST['not_null'][$itr] != 'DEFAULT')
                    $str = $str."-@-".json_decode(json_encode($_POST['not_null'][$itr]));

                if ( !empty($_POST['unique_key']) && isset($_POST['unique_key'][$itr]) && $_POST['unique_key'][$itr] != 'DEFAULT')
                    $str = $str."-@-".json_decode(json_encode($_POST['unique_key'][$itr]));

                if ( !empty($_POST['matching_regex']) && isset($_POST['matching_regex'][$itr]) && $_POST['matching_regex_inputs'][$itr] != 'Your_Input_Here') {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['matching_regex'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['matching_regex_inputs'][$itr])) . "]";
                }

                if ( !empty($_POST['string_constraint']) && isset($_POST['string_constraint'][$itr]) && $_POST['satisfies_string_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['string_constraint'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['satisfies_string_inputs'][$itr])) . "]";
                }

                if ( !empty($_POST['num_rows']) && isset($_POST['num_rows'][$itr]) && $_POST['num_rows_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['num_rows'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['num_rows_inputs'][$itr])) . "]";
                }
                if ( !empty($_POST['any_of']) && isset($_POST['any_of'][$itr]) && $_POST['any_of_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['any_of'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['any_of_inputs'][$itr])) . "]";
                }

                if ( !empty($_POST['formatted_date']) && isset($_POST['formatted_date'][$itr]) && $_POST['formatted_date_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['formatted_date'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['formatted_date_inputs'][$itr])) . "]";
                }

                if ( !empty($_POST['convertible_datatype']) && isset($_POST['convertible_datatype'][$itr]) && $_POST['convertible_datatype_inputs'][$itr] != 'Your_Input_Here') {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['convertible_datatype'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['convertible_datatype_inputs'][$itr])) . "]";
                }

                if ( !empty($_POST['joinable_with']) && isset($_POST['joinable_with'][$itr]) && $_POST['joinable_with_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['joinable_with'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['joinable_with_inputs'][$itr])) . "]";
                }

                if ( !empty($_POST['equallable_with']) && isset($_POST['equallable_with'][$itr]) && $_POST['equallable_with_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['equallable_with'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['equallable_with_inputs'][$itr])) . "]";
                }
                if ( !empty($_POST['foreign_key']) && isset($_POST['foreign_key'][$itr]) && $_POST['foreign_key_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['foreign_key'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['foreign_key_inputs'][$itr])) . "]";
                }

                if ( !empty($_POST['functional_dependency']) && isset($_POST['functional_dependency'][$itr]) && $_POST['functional_dependency_inputs'][$itr] != 'Your_Input_Here' ) {
                    $str = $str . "-@-" . json_decode(json_encode($_POST['functional_dependency'][$itr]));
                    $str = $str . "[" . json_decode(json_encode($_POST['functional_dependency_inputs'][$itr])) . "]";
                }

                $str .= "--@@--";
                $itr++;

                $data .= $str;
                echo "<br/>";
                echo $data;
                echo "<br/>";
            }
            echo "Final Value is ".$data;
            $i++;
        }

        // Rest Call to the Specified Azkaban URL :
        $Login_URL = 'http://35.154.225.107:8081';
        $Login_Data = array('action' => 'login', 'username' => 'azkaban', 'password' => 'azkaban');

        // use key 'http' even if you send the request to https://...
        $options = array( 'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST','content' => http_build_query($Login_Data)
                        )   );
        $result = file_get_contents($Login_URL, false, stream_context_create($options));

        $session_details = json_decode($result, true);

         if ($session_details["status"] == 'success') {
            $session_id = $session_details["session.id"];

            $FLOW_TRIGGER_URL = 'http://35.154.225.107:8081/executor';

            $FLOW_TRIGGER_DATA = array('ajax' => 'executeFlow', 'session.id' => $session_id, 'project' => 'DQ_Web_UI', 'flow' => 'of-dq-runner', 'flowOverride[JSON_SCHEMA]' => $data );
            $FLOW_TRIGGER_OPTIONS = array('http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST', 'content' => http_build_query($FLOW_TRIGGER_DATA)
                )   );

            $FLOW_TRIGGER_RESULT = file_get_contents($FLOW_TRIGGER_URL, false, stream_context_create($FLOW_TRIGGER_OPTIONS));

            $execution_details = json_decode($FLOW_TRIGGER_RESULT, true);
            $execution_id = $execution_details["execid"];


            $gotoURL = $FLOW_TRIGGER_URL."?execid=".$execution_id;

            echo "<br/>";
            print_r($gotoURL);

            //Persist the details of the flow
            $con = mysqli_connect('127.0.0.1','root','','dq_webui');
            if (!$con) {
                die('Could not connect to the mysql Instance' . mysqli_error($con));
            }

            $lastRunOn = date("Y/m/d h:i:sa");
            $sql = "INSERT INTO historical_extracts (executionId,modelName,modelSchema,schedulingType,lastRunOn,raw_schema) VALUES ('$execution_id', 'Not Defined', '', 'CONSOLE-START', '$lastRunOn','$data') ";

            if ($con->query($sql) === TRUE) {
                //Take it to the Scheduled Models Page
                echo "<br/> Successfully Persisted";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            header("location:register-dq-model-confirmation.php");
    }

    }
?>