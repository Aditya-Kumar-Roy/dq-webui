<!DOCTYPE html>
<html>
<head>
    <title>Exadatum's Data Quality Tool</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <link rel="stylesheet" type="text/css" href="../commons/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../commons/css/keen-dashboards.css" />
    <link rel="stylesheet" type="text/css" href="register-dq-model-confirmation.css" />
    <link rel="stylesheet" type="text/css" href="add-columns.js" />
    <script type="text/javascript" src="register-dq-model.js"></script>
</head>
<body class="application">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="">
                <img src="../../img/exa-logo.png" width="20">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="navbar-brand" href="./">Exadatum's Data Quality Tool</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="../index-files/index.html">My DashBoard</a></li>
                <li><a href="register-dq-model.html">Register DQ Model</a></li>
                <li><a href="../past-dq-models/past-dq-models.php">Past DQ Model</a></li>
                <li><a href="../schedule-dq-models/scheduled-models.html">Scheduled DQ Model</a></li>
                <li><a href="../undercons.html">History Logs</a></li>
                <li><a href="http://35.154.225.107:5601/app/kibana#/management/kibana/index/?_g=()"> Kibana Dashboard</a> </li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <br><br> <br> <br> <br>
    <fieldset>
        <legend> <strong> Message from server </strong> </legend>
        <strong> Data Quality Model Successfully Created. Visit Kibana Board for visulizations !!</strong>

        <hr>
        <br/>
        <table id="table2" border="0" width="500" align="center" class="demo-table">
            <tr>
            <td align="center"> <a href="http://ec2-35-154-53-50.ap-south-1.compute.amazonaws.com:8088/cluster"> <input type="button" value="View Yarn Logs" name="addColumn" class="btnAddColumn"> </a> </td>
            <td align="center"> <a href="http://35.154.225.107:5601/app/kibana#/management/kibana/index/?_g=()"> <input type="button" value="View Kibana Metrices" name="deleteColumn" class="btnDeleteColumn"> </a> </td>
                <td align="center"> <a href="http://127.0.0.1:4040"> <input type="button" value="View Spark UI" name="deleteColumn" class="btnDeleteColumn">  </a> </td>
                <td align="center"> <a href="http://127.0.0.1:9200"> <input type="button" value="View Elastic Search Indexes" name="deleteColumn" class="btnDeleteColumn"> </a> </td>
            </tr>
        </table>
    </fieldset>

    <hr>
<p class="small text-muted">Built with &#9829; by <a href="https://www.exadatum.com">Exadatum Software Services Pvt. Ltd</a></p>

</div>

<script type="text/javascript" src="../commons/jquery.min.js"></script>
<script type="text/javascript" src="../commons/bootstrap.min.js"></script>

<script type="text/javascript" src="../commons/holder.js"></script>
<script>
    Holder.add_theme("white", { background:"#fff", foreground:"#a7a7a7", size:10 });
</script>

<script type="text/javascript" src="../commons/keen.min.js"></script>
<script type="text/javascript" src="../commons/meta.js"></script>
<script type="text/javascript" src="../commons/keen.dashboard.js"></script>
</body>
</html>
