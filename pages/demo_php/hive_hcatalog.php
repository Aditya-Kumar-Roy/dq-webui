<?php
$GLOBALS['THRIFT_ROOT'] = dirname(__FILE__).'/thrift/src';
require_once( $GLOBALS['THRIFT_ROOT'].'/Thrift.php' );
require_once( $GLOBALS['THRIFT_ROOT'].'/transport/TSocket.php' );
require_once( $GLOBALS['THRIFT_ROOT'].'/transport/TBufferedTransport.php' );
require_once( $GLOBALS['THRIFT_ROOT'].'/protocol/TBinaryProtocol.php' );

//hbase thrift
require_once dirname(__FILE__).'/thrift/Hbase.php';

//hive thrift
require_once dirname(__FILE__).'/thrift/ThriftHive.php';

/*
HBase php thrift client
*/

//open connection
$socket = new TSocket( 'localhost', 9090 );
$transport = new TBufferedTransport( $socket );
$protocol = new TBinaryProtocol( $transport );
$client = new HbaseClient( $protocol );
$transport->open();

//show all tables
$tables = $client->getTableNames();
foreach ( $tables as $name ) {
echo( "  found: {$name}\n" );
}

//Create a table
try {
$columns = array(new ColumnDescriptor( array(
'name' =>; 'colFamily:',
'maxVersions' => 10) ));

$client->createTable("tableName", $columns );
} catch ( AlreadyExists $ae ) {
echo( "WARN: {$ae->;message}\n" );
}

//insert data to table
$mutations = array(
new Mutation( array(
'column' => 'colFamily:Col',
'value' => 'value123'
) ),
);
$client->mutateRow( "tableName", "ID_1237846634624", $mutations );

//get table data
$row = $client->getRow("tableName", "ID_1237846634624");

/*
Hive php thrift client
*/

//open connection
$transport = new TSocket("localhost", 10000);
$protocol = new TBinaryProtocol($transport);
$client = new ThriftHiveClient($protocol);
$transport->open();

//show tables
$client->execute('SHOW TABLES');
$tables = $client->fetchAll();
foreach ($tables as $name){
echo( " found: {$name}\n" );
}

//Create Hive table with Hbase table mapping
$mapping = 'CREATE EXTERNAL TABLE tableName(Col String, Col1 String)

STORED BY \'org.apache.hadoop.hive.hbase.HBaseStorageHandler\'
WITH SERDEPROPERTIES ("hbase.columns.mapping" = "colFamily:Col, colFamily:Col")
TBLPROPERTIES("hbase.table.name" = "tableName")';

$client->execute($mapping);

//Query table
$client->execute('SELECT * FROM tableName Limit 10');
var_dump($client->fetchAll());

?>